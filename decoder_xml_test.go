package rdecoder_test

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rdecoder"
)

func TestXMLContentType(t *testing.T) {
	decd := rdecoder.NewXMLEncoder()

	res := decd.ContentType()

	assert.Equal(t, "application/xml", res, "[TestXMLContentType] Content type should \"application/xml\"")
}

func TestXMLDecode(t *testing.T) {
	decd := rdecoder.NewXMLEncoder()

	var rdr io.Reader = bytes.NewBufferString(`
	<note>
		<to>Tove</to>
	</note>
	`)

	type Note struct {
		To string `xml:"to"`
	}

	des := Note{}

	res := decd.Decode(rdr, &des)

	assert.NoError(t, res, "[TestXMLDecode] Result should nil")
}

func TestXMLEncode(t *testing.T) {
	decd := rdecoder.NewXMLEncoder()

	var wtr io.Writer = bytes.NewBufferString(`
	<note>
		<to>Tove</to>
	</note>
	`)

	type Note struct {
		To string `xml:"to"`
	}

	des := Note{}

	res := decd.Encode(wtr, &des)

	assert.NoError(t, res, "[TestXMLEncode] Result should nil")
}

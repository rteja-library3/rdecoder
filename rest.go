package rdecoder

import (
	"net/http"

	"gitlab.com/rteja-library3/rresponser"
)

func DecodeRest(r *http.Request, dec Decode, res interface{}) (err error) {
	err = dec.Decode(r.Body, res)
	return
}

func EncodeRestWithResponser(w http.ResponseWriter, enc Encode, res rresponser.Responser) (err error) {
	w.Header().Set("Content-type", enc.ContentType())
	w.WriteHeader(res.HTTPStatus())

	data := map[string]interface{}{
		"code":    res.Code(),
		"message": res.Message(),
		"data":    res.Data(),
		"meta":    res.Meta(),
		"success": res.Error() == nil,
	}

	err = enc.Encode(w, data)
	return
}

package rdecoder

import (
	"encoding/json"
	"io"
)

type jsonEncoder struct{}

func NewJSONEncoder() Decoder {
	return &jsonEncoder{}
}

func (j jsonEncoder) Decode(r io.Reader, res interface{}) (err error) {
	err = json.NewDecoder(r).Decode(res)
	return
}

func (j jsonEncoder) Encode(w io.Writer, re interface{}) (err error) {
	err = json.NewEncoder(w).Encode(re)
	return
}

func (j jsonEncoder) ContentType() string {
	return "application/json"
}

package rdecoder_test

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rdecoder"
)

func TestJSONContentType(t *testing.T) {
	decd := rdecoder.NewJSONEncoder()

	res := decd.ContentType()

	assert.Equal(t, "application/json", res, "[TestJSONContentType] Content type should \"application/json\"")
}

func TestJSONDecode(t *testing.T) {
	decd := rdecoder.NewJSONEncoder()

	var rdr io.Reader = bytes.NewBufferString("{\"id\": 1}")
	var des map[string]interface{}

	res := decd.Decode(rdr, &des)

	assert.NoError(t, res, "[TestJSONDecode] Result should nil")
}

func TestJSONEncode(t *testing.T) {
	decd := rdecoder.NewJSONEncoder()

	var wtr io.Writer = bytes.NewBufferString("{\"id\": 1}")
	var des map[string]interface{}

	res := decd.Encode(wtr, &des)

	assert.NoError(t, res, "[TestJSONEncode] Result should nil")
}

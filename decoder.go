package rdecoder

import (
	"io"
)

type Decode interface {
	Decode(r io.Reader, res interface{}) error
}

type Encode interface {
	Encode(w io.Writer, re interface{}) error
	ContentType() string
}

type Decoder interface {
	Decode
	Encode
}

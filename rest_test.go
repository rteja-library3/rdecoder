package rdecoder_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rdecoder"
	"gitlab.com/rteja-library3/rdecoder/mocks"
	rresponsermocks "gitlab.com/rteja-library3/rresponser/mocks"
)

func TestDecodeRest(t *testing.T) {
	data := map[string]interface{}{}

	request, _ := http.NewRequest(http.MethodPost, "/9908-test", nil)

	dec := new(mocks.Decoder)

	dec.On("Decode", mock.Anything, mock.Anything).
		Return(nil)

	err := rdecoder.DecodeRest(request, dec, &data)

	assert.NoError(t, err, "[TestDecodeRest] Should not nil")
}

func TestEncodeRestWithResponser(t *testing.T) {
	data := new(rresponsermocks.Responser)

	data.On("HTTPStatus").
		Return(200)

	data.On("Code").
		Return("success")

	data.On("Message").
		Return("Successfully created data")

	data.On("Data").
		Return(nil)

	data.On("Meta").
		Return(nil)

	w := httptest.NewRecorder()

	dec := new(mocks.Decoder)

	dec.On("Encode", mock.Anything, mock.Anything).
		Return(nil)

	dec.On("ContentType").
		Return("application/json")

	err := rdecoder.EncodeRestWithResponser(w, dec, data)

	assert.NoError(t, err, "[TestEncodeRestWithResponser] Should not nil")
}

package rdecoder

import (
	"encoding/xml"
	"io"
)

type xmlEncoder struct{}

func NewXMLEncoder() Decoder {
	return &xmlEncoder{}
}

func (x xmlEncoder) Decode(r io.Reader, res interface{}) (err error) {
	err = xml.NewDecoder(r).Decode(res)
	return
}

func (x xmlEncoder) Encode(w io.Writer, re interface{}) (err error) {
	err = xml.NewEncoder(w).Encode(re)
	return
}

func (j xmlEncoder) ContentType() string {
	return "application/xml"
}
